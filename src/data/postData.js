const postData = [
    {
        id: 1,
        title: "What are some decent paying jobs you can get if you were a C student in highschool?",
        subTalk: "Programming",
        points: "23",
        user: "JustinIsCheating",
        time: "6 hours ago",
        subTalkId: 1
    },
    {
        id: 2,
        title: "LPT: Learn what to stockpile in case of plague, earthquake, blizzard, or other major events. You probably don't need to hit the freezer section of your local store.",
        subTalk: "Funny",
        points: "11",
        user: "Sympatic",
        time: "6 hours ago",
        subTalkId: 2
    },
    {
        id: 3,
        title: "This machine visualizes number googol (a 1 with a 100 zeros, bigger than the atoms in the known universe) & has a gear reduction of 1 to 10 a hundred times. To get last gear to turn once you'll need to spin first one a googol amount around, which will require more energy than entire universe has.",
        subTalk: "NBA",
        points: "17",
        user: "Lorenzo",
        time: "4 hours ago",
        subTalkId: 3
    },
    {
        id: 4,
        title: "Bernie Sanders wins Democratic presidential primary in California, claiming biggest prize on Super Tuesday.",
        subTalk: "Dutchies",
        points: "19",
        user: "Ian.K4rs",
        time: "3 hours ago",
        subTalkId: 4
    },
    {
        id: 5,
        title: "LPT: Learn what to stockpile in case of plague, earthquake, blizzard, or other major events. You probably don't need to hit the freezer section of your local store.",
        subTalk: "Funny",
        points: "11",
        user: "Sympatic",
        time: "6 hours ago",
        subTalkId: 2
    },
    {
        id: 6,
        title: "What are some decent paying jobs you can get if you were a C student in highschool?",
        subTalk: "Programming",
        points: "23",
        user: "JustinIsCheating",
        time: "6 hours ago",
        subTalkId: 1
    }
]

export default postData
