const userData = [
    {
        id: 1,
        name: 'Ian Kars',
        username: 'Ian.K4rs',
        email: 'giant@solcon.nl',
        subTalks: [1, 4]
    }
]

export default userData
