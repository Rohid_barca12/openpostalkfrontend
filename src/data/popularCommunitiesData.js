const popularCommunitiesData = [
    {
        id: 1,
        name: "Funny"
    },
    {
        id: 2,
        name: "Programming"
    },
    {
        id: 3,
        name: "History",
    },
    {
        id: 4,
        name: "NBA",
    },
    {
        id: 5,
        name: "Dutchies",
    }
]

export default popularCommunitiesData
