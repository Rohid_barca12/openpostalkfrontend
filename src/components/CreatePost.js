//import react
import React, {Component} from 'react'
import { Redirect } from "react-router-dom"

// import axios
import axios from 'axios';

class CreatePost extends Component {

    constructor() {
        super()
        this.state = {
            title: null,
            content: null,
            categoryId: null,
            subtalkList: [],
            redirect: false,
            redirectId: null
        }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.handlePostCreate = this.handlePostCreate.bind(this)
    }

    async componentDidMount() {
        await axios.get('http://localhost:3000/category', {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/x-www-form-urlencoded'
            }})
            .then(response => {
                this.setState({
                    subtalkList: response.data
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name
        let errors = this.state.errors

        this.setState({
            errors, [name]: value
        })
    }

    async handlePostCreate(event) {
        event.preventDefault()
        
        await axios.post('http://localhost:3000/post', {
                title: this.state.title,
                content: this.state.content,
                categoryId: parseInt(this.state.categoryId)
            },
            {
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token'),
                    'Content-Type': 'application/json'
                }
            })
        .then(response => {
            this.setState({
                redirect: true,
                redirectId: response.data.id
            })
        })
        .catch(error => {
            this.setState({
                error: error.response.data
            })

            if (this.state.error.statusCode !== 400) {
                console.log(this.state.error)
            }
        })
    }

    render() {

        if (this.state.redirect) {
            return <Redirect to={`/post/${this.state.redirectId}`} />
        }

        return (
            <div className="col-md-9 border-md-left py-4">
                <h2 className="font-weight-bold mb-4">Create an awesome post!</h2>
                <form onSubmit={this.handlePostCreate}>
                    <div className="form-group">
                        <label className="m-0" htmlFor="title">Title</label>
                        <input className="form-control" type="text" id="title" name="title" placeholder="The title of your new post" onChange={this.handleInputChange} />
                    </div>
                    <div className="form-group">
                        <label className="m-0" htmlFor="content">Content</label>
                        <textarea className="form-control" id="content" name="content" placeholder="What would you like to share with us?" onChange={this.handleInputChange} ></textarea>
                    </div>
                    <div className="form-group">
                        <label className="m-0" htmlFor="categoryId">Subtalk</label>
                        <select className="form-control" id="categoryId" name="categoryId" onChange={this.handleInputChange}>
                            <option>-- Select who you would like to share this post with --</option>
                            {
                                this.state.subtalkList.map(category => 
                                    <option key={category.id} value={category.id}>{category.name}</option>
                                )
                            }
                        </select>
                    </div>
                    <div className="form-group text-right">
                        <button className="btn btn-success btn-fill" type="submit">Create post</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default CreatePost
