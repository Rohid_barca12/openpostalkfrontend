//import react
import React, {Component} from 'react'

// import axios
import axios from 'axios';

class User extends Component {

    constructor(props) {
        super(props)
        this.state = {
            firstName: null,
            lastName: null,
            username: null,
            email: null
        }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async componentDidMount() {

        await axios.get(`http://localhost:3000/user/${isNaN(this.props.userId) ? 'current' : this.props.userId}`, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/json'
            }})
            .then(response => {
                this.setState({
                    firstName: response.data.firstName,
                    lastName: response.data.lastName,
                    username: response.data.username,
                    email: response.data.email
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value
        })
    }

    handleSubmit(event) {
        alert('User updated')
        event.preventDefault()
    }

    render() {
        return (
            <div className="generalUser2 col-md-9 py-4 border-md-left">
                <h2 className="font-weight-bold mb-4">User information</h2>
                <form>
                    <div className="form-group">
                        <label className="m-0" htmlFor="firstName">First name</label>
                        <input className="firstNameChange form-control" type="text" id="firstName" name="firstName" value={this.state.firstName} disabled />
                    </div>
                    <div className="form-group">
                        <label className="m-0" htmlFor="lastName">Last name</label>
                        <input className="lastNameChange form-control" type="text" id="lastName" name="lastName" value={this.state.lastName} disabled />
                    </div>
                    <div className="form-group">
                        <label className="m-0" htmlFor="username">Username</label>
                        <input className="usernameChange form-control" type="text" id="username" name="username" value={this.state.username} disabled />
                    </div>
                    <div className="form-group">
                        <label className="m-0" htmlFor="email">E-mail</label>
                        <input className="emailDark form-control" type="email" id="email" name="email" value={this.state.email} disabled />
                    </div>
                    <div className="form-group text-right">
                        <button className="btn btn-success btn-fill" type="submit">Update</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default User
