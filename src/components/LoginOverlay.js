//import react
import React, {Component} from 'react'

// import axios
import axios from 'axios';

class LoginOverlay extends Component {

    constructor()
    {
        super()
        this.state = {
            loading: false,
            redirect: false,
            loginFailed: false,
            username: '',
            password: ''
        }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleLogin = this.handleLogin.bind(this)
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value
        })
    }

    async handleLogin(event) {
        event.preventDefault()
        this.setState({
            loading: true,
        })
        await axios.post('http://localhost:3000/auth/login', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                this.setState({
                    redirect: true,
                    loginFailed: false
                })
                localStorage.setItem('token', response.data.access_token)
                this.props.handleIsLoggedIn()
            })
            .catch(error => {
                this.setState({
                    loading: false,
                    loginFailed: true
                })
            })
    }

    render() {
        return (
            <div className="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalCenterTitle">Login to follow this subtalk</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={this.handleLogin}>
                                <div className="form-group">
                                    <label className="m-0" htmlFor="username">Username</label>
                                    <input className="form-control" type="text" id="username" name="username" onChange={this.handleInputChange} />
                                </div>
                                <div className="form-group">
                                    <label className="m-0" htmlFor="password">Password</label>
                                    <input className="form-control" type="password" id="password" name="password" onChange={this.handleInputChange} />
                                </div>
                                <div className="form-group text-right">
                                    {
                                        this.state.loading ?
                                            <button className="btn btn-success btn-fill" type="submit" disabled><i className="fas fa-spinner fa-spin mr-1"></i> Log in</button>
                                        :
                                            <button className="btn btn-success btn-fill" type="submit" data-dismiss="modal">Log in</button>
                                    }
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default LoginOverlay
