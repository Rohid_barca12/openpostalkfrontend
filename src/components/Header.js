// import react
import React, {Component} from 'react'
import { Link } from "react-router-dom"

// import axios
import axios from 'axios';

// import assets
import logoDark from '../img/logo_white.png'
import logoLight from '../img/logo.png'

class Header extends Component {

    constructor() {
        super()
        this.state = {
            search: '',
            name: '',
            username: '',
            email: '',
            roleId: null
        }

        this.onSearchChange = this.onSearchChange.bind(this)
        this.updateHeader = this.updateHeader.bind(this)
    }

    async componentDidMount() {
        if (localStorage.getItem('token') !== null && localStorage.getItem('token') !== '') {
            await axios.get('http://localhost:3000/user/current?entities=1', {
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('token'),
                    "Content-Type": "application/json"
                }})
                .then(response => {
                    this.setState({
                        name: response.data.firstName + ' ' + response.data.lastName,
                        username: response.data.username,
                        email: response.data.email,
                        roleId: response.data.role !== null ? response.data.role.id : null
                    })
                })
                .catch(error => {
                    console.log(error)
                })
        }
    }

    async updateHeader() {
        if (localStorage.getItem('token') !== null && localStorage.getItem('token') !== '') {
            await axios.get('http://localhost:3000/user/current?entities=1', {
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('token'),
                    "Content-Type": "application/json"
                }})
                .then(response => {
                    this.setState({
                        name: response.data.firstName + ' ' + response.data.lastName,
                        username: response.data.username,
                        email: response.data.email,
                        roleId: response.data.role !== null ? response.data.role.id : null
                    })
                })
                .catch(error => {
                    console.log(error)
                })
        }
    }

    onSearchChange(event) {
        this.setState({
            search: event.target.value
        })
        console.log(this.state.search)
    }

    onSearchSubmit(event) {
        alert(this.state.search)
        event.preventDefault()
    }

    render() {
        return (
            <header className="py-3">
                <div className="container">
                    <div className="row d-flex justify-content-between align-items-center">
                        <div className="col-5 col-sm-6 col-md-4 col-lg-3 text-white">
                            <Link to="/"><img className="logo" src={this.props.darkBool ? logoDark : logoLight} alt="Logo Postalk" /></Link>
                        </div>
                        <nav className="col-7 col-sm-6 col-md-8 col-lg-4">
                            <ul className="d-flex justify-content-end align-items-center list-unstyled m-0 p-0">
                                {
                                    localStorage.getItem('token') === null || localStorage.getItem('token') === '' || localStorage.getItem('token') === 'undefined'
                                    ?
                                        <React.Fragment>
                                            <li><Link to="/register" className="btn btn-primary btn-fill py-1 px-3">Register</Link></li>
                                            <li><Link to="/login" className="btn btn-secondary btn-ghost py-1 px-3 ml-2">Log in</Link></li>
                                            
                                        </React.Fragment>
                                    :
                                        <React.Fragment>
                                            <li>
                                                <span className="user-btn px-3 py-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i className="fas fa-user mr-1"></i>
                                                    Create
                                                    <i className="fas fa-caret-down ml-2"></i>
                                                </span>
                                                <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <Link to="/create/post" className="dropdown-item">
                                                        New Post
                                                    </Link>
                                                    <Link to="/create/subtalk" className="dropdown-item">
                                                        New Subtalk
                                                    </Link>
                                                </ul>
                                            </li>
                                            <li className="ml-2">
                                                <span className="user-btn px-3 py-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i className="fas fa-user mr-2"></i>
                                                    <i className="fas fa-caret-down"></i>
                                                </span>
                                                <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <li className="dropdown-item no-hover">
                                                        <b>{this.state.name}</b><br />
                                                        {this.state.email}
                                                    </li>
                                                    <hr className="my-2" />
                                                    <Link to="/user/general" className="dropdown-item">
                                                        My account
                                                    </Link>
                                                    {
                                                        this.state.roleId === 1 &&
                                                            <Link to="/admin/users" className="dropdown-item">
                                                                Admin page
                                                            </Link>
                                                    }
                                                    <Link to="/" className="dropdown-item" onClick={this.props.handleLogOut}>
                                                        Log out
                                                    </Link>
                                                </ul>
                                            </li>
                                        </React.Fragment>
                                }
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header
