//import react
import React from 'react'

function SubtalkDescription(props) {
    return (
        <div className="sidebar my-3">
            <div className="title px-3 py-3">
                <b className="text-white">About {props.subtalk.name}</b>
            </div>
            <div className="sidebar-body p-3">
                {props.subtalk.description}
            </div>
        </div>
    )
}

export default SubtalkDescription
