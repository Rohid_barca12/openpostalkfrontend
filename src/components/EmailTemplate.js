import React, { Component } from "react";
import logo from '../img/logo-icon.png';

import axios from "axios";

class EmailTemplate extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstName: '',
            lastName: ''
            
        }
    }

    async componentDidMount() {
        await axios.get(`http://localhost:3000/user/${isNaN(this.props.userId) ? 'current' : this.props.userId}`, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/json'
            }})
            .then(response => {
                this.setState({
                    firstName: response.data.firstName,
                    lastName: response.data.lastName
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        return (
            <div className="col-md-9 py-4 border-md-left">
                <h2 className="font-weight-bold mb-4">Email Template</h2>
                    <article>
                        <p>
                            Beste,
                        </p>
                        <div>
                            <textarea className="form-control"></textarea>
                        </div>
                        <p>
                        <br/>
                            Met vriendelijke groet,
                        </p>
                        <p>
                            The Postalk team<br /> 
                            <img className="logoEmail" alt="logo" src={logo} />
                        </p>
                    </article>

                <button className="btn btn-success btn-fill">Verzenden</button>
            </div>
        )
    }
}

export default EmailTemplate;