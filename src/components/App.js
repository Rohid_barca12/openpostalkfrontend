// import react
import React, {Component} from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom"

import '../assets/light.css'
import '../assets/dark.css'

// import components
import Header from './Header'

// import pages
import Home from '../pages/Home'
import Register from '../pages/Register'
import Login from '../pages/Login'
import Post from '../pages/Post'
import User from '../pages/User'
import Forbidden from '../pages/Forbidden'
import Create from '../pages/Create'
import Subtalk from '../pages/Subtalk'
import Communities from '../pages/Communities'
import Admin from '../pages/Admin'
import Error from '../pages/Error'
import NotFound from '../pages/NotFound'

class App extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoggedIn: false,
            dark: false,
        }

        this.handleIsLoggedIn = this.handleIsLoggedIn.bind(this)
        this.handleLogOut = this.handleLogOut.bind(this)
        this.handleStyleChange = this.handleStyleChange.bind(this)
    }

    componentDidMount() {
        this.setState({
            isLoggedIn: false
        })
    }

    handleIsLoggedIn() {
        this.setState({
            isLoggedIn: true
        })
        
        this.refs.header.updateHeader()
    }

    handleLogOut() {
        localStorage.clear()
        this.setState({
            isLoggedIn: false,
            dark: false
        })
    }

    handleStyleChange() {
        this.setState(
            prevState => {
                return {
                    dark: !prevState.dark
                }
            }
        )
    }

    render() {
        return (
            <div id={this.state.dark ? 'darkTheme' : 'lightTheme'}>
                <Router>
                    <Header handleLogOut={this.handleLogOut} darkBool={this.state.dark} ref="header" />

                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route path="/post/:id" component={Post} />
                        <Route path="/subtalk/:id" component={Subtalk} />
                        <Route path="/login">
                            <Login handleIsLoggedIn={this.handleIsLoggedIn} />
                        </Route>
                        <Route path="/register">
                            <Register handleIsLoggedIn={this.handleIsLoggedIn} />
                        </Route>
                        <Route path="/communities" component={Communities} />
                        <Route path="/error/:handle" component={Error} />
                        <Route path="/user/:handle/:userId?" render={(props) => <User {...props} handleStyleChange={this.handleStyleChange} darkBool={this.state.dark} />} />
                        <Route path="/create/:handle" component={Create} />
                        <Route path="/admin/:handle" component={Admin} />
                        <Route path="/forbidden" component={Forbidden} />
                        <Route component={NotFound} />
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default App
