//import react
import React, {Component} from 'react'

// import axios
import axios from 'axios';

class UserPasswordChange extends Component {

    constructor() {
        super()
        this.state = {
            oldPassword: '',
            newPassword: '',
            confirmPassword: '',
            update_success: false,
            update_failed: false,
            errorMessages: []
        }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value
        })
    }

    async handleSubmit(event) {
        event.preventDefault()

        await axios.patch('http://localhost:3000/user/current/updatePassword', {
            oldPassword: this.state.oldPassword,
            newPassword: this.state.newPassword,
            confirmNewPassword: this.state.confirmPassword
        },
        {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/json'
            }
        })
        .then(response => {
            this.setState({
                update_success: true
            })
        })
        .catch(error => {
            this.setState({
                update_failed: true,
                errorMessages: error.message
            })
        })
    }

    render() {
        return (
            <div className="userInfo col-9 border-md-left py-4">
                <h2 className="font-weight-bold mb-4">User information</h2>

                {this.state.update_success && <span className="alert alert-success" role="alert">Password successfully updated</span>}
                {this.state.update_failed && <span className="alert alert-danger" role="alert">{this.state.errorMessages}</span>}
                
                <form onSubmit={this.handleSubmit} className="mt-4">
                    <div className="form-group">
                        <label className="passwordChange m-0" htmlFor="oldPassword">Old Password</label>
                        <input className="form-control" type="password" id="oldPassword" name="oldPassword" value={this.state.oldPassword} onChange={this.handleInputChange} />
                    </div>
                    <div className="form-group">
                        <label className="lastNameChange m-0" htmlFor="newPassword">New Password</label>
                        <input className="form-control" type="password" id="newPassword" name="newPassword" value={this.state.newPassword} onChange={this.handleInputChange} />
                    </div>
                    <div className="form-group">
                        <label className="usernameChange m-0" htmlFor="confirmPassword">Confirm Password</label>
                        <input className="form-control" type="password" id="confirmPassword" name="confirmPassword" value={this.state.confirmPassword} onChange={this.handleInputChange} />
                    </div>
                    <div className="form-group text-right">
                        <button className="btn btn-success btn-fill" type="submit">Update</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default UserPasswordChange
