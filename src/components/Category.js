// import React
import React from 'react'

function Category(props) {
    return (
        <li className="list-item px-3 p-2" onClick={() => props.filterCategory()}>Gaming</li>
    )
}

export default Category
