// import react
import React from 'react'
import { Link } from "react-router-dom"

function Post(props) {
    const createdAt = props.post.createdAt

    return (
        <div className="content-container d-flex text-decoration-none my-3">
            <div className="post-votes d-flex flex-column align-items-center text-center py-2"></div>
            <div className="content">
                <div className="content-body">
                    <Link className="text-decoration-none" to={`/post/${props.post.id}`}>
                        <h5 className="font-weight-bold">{props.post.title}</h5>
                        <p>
                            {props.post.content}
                        </p>
                    </Link>
                </div>
                <div className="content-actions d-flex align-items-center">
                    <Link className="mr-1 px-2" to={`/post/${props.post.id}`}><i className="fas fa-comment-alt fa-xs"></i> Comments</Link>
                </div>
            </div>
        </div>
    )

}

export default Post
