//import react
import React, {Component} from 'react'

// import axios
import axios from 'axios';

class Users extends Component {

    constructor() {
        super()
        this.state = {
            userId: null,
            users: [],
            error: false,
            errorMessage: ''
        }

        this.deleteUser = this.deleteUser.bind(this)
        this.getUsers = this.getUsers.bind(this)
        this.getUsersFromState = this.getUsersFromState.bind(this)
    }

    async componentDidMount() {
        await axios.get('http://localhost:3000/user?entities=1', {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/json'
            }})
            .then(response => {
                this.setState({
                    users: response.data
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    async getUsers() {
        await axios.get('http://localhost:3000/user?entities=1', {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/json'
            }})
            .then(response => {
                this.setState({
                    users: response.data
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    getUsersFromState() {
        const users = this.state.users

        return users.map(user =>
            <tr key={user.id}>
                <td>{user.firstName + ' ' + user.lastName}</td>
                <td>{user.username}</td>
                <td>{user.email}</td>
                <td className="text-right">
                    {
                        user.role === null &&
                            <div className="btn-group">
                                <button to="/register" className="btn btn-danger border-0 py-1 px-3" onClick={() => {this.toggleOverlay(user.id)}} >Delete user</button>
                            </div>
                    }
                </td>
            </tr>
        )
    }

    async deleteUser(id) {
        await axios.delete(`http://localhost:3000/user/${id}`, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/json'
            }})
            .then(response => {
                this.getUsers()
                this.setState({
                    showOverlay: false
                })
            })
            .catch(error => {
                this.setState({
                    error: true,
                    errorMessage: error.message
                })
            })
    }

    toggleOverlay(userId) {
        this.setState(prevstate => {
            return {
                showOverlay: !prevstate.showOverlay,
                userToDelete: userId
            }
        })
    }

    render() {
        return (
            <div className="col-md-9 py-4 border-md-left">
                <h2 className="font-weight-bold mb-4">Users</h2>
                
                {this.state.error && <span className="alert alert-danger" role="alert">{this.state.errorMessage}</span>}

                <table className="table w-100 mt-4">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>E-mail</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.getUsersFromState()}
                    </tbody>
                </table>

                {
                    this.state.showOverlay &&
                        <div className="overlay text-center p-4">
                            <h3 className="font-weight-bold mb-3">Are you sure you want to delete this user?</h3>

                            <button 
                                className="btn btn-danger border-0" 
                                onClick={() => {this.deleteUser(this.state.userToDelete)}} 
                            >
                                Delete
                            </button>
                            
                            <button 
                                className="btn btn-secondary border-0 ml-2" 
                                onClick={() => {this.toggleOverlay(null)}} 
                            >
                                Cancel
                            </button>
                        </div>
                }
            </div>
        )
    }
}

export default Users
