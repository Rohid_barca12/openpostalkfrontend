// import react
import React, {Component} from 'react'
import { Link } from "react-router-dom"

class PopularCommunitiesItem extends Component {

    render() {
        return (
            <Link to={`/subtalk/${this.props.category.id}`}>
                <div className="list-item px-3 py-2">
                    {this.props.category.name}
                </div>
            </Link>
        )
    }
}

export default PopularCommunitiesItem
