//import react
import React, {Component} from 'react'

// import axios
import axios from 'axios';

class Reports extends Component {

    constructor() {
        super()
        this.state = {
            reports: [
                {
                    id: '1',
                    content: 'test',
                    userId: '1',
                    categoryId: null,
                    postId: null,
                    commentId: null
                },
                {
                    id: '2',
                    content: 'testen',
                    userId: null,
                    categoryId: '4',
                    postId: null,
                    commentId: null
                }
            ],
            userReports: [],
        }

        this.getReports = this.getReports.bind(this)
        this.sortReports = this.sortReports.bind(this)
    }

    async componentDidMount() {
        await axios.get('http://localhost:3000/report', {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/json'
            }})
            .then(response => {
                this.setState({
                    reports: response.data
                })
            })
            .catch(error => {
                console.log(error)

                this.sortReports()
            })
    }

    sortReports() {
        const reports = this.state.reports

        var userReports = reports.map(report => function() {
            console.log(report)
            if (report.userId !== null) {
                this.setState(prevState => ({
                    userReports: prevState.userReports.concat(report)
                }))
            }
        })

        console.log(userReports)
    }

    getReports() {
        const users = this.state.users

        return users.map(user =>
            <tr key={user.id}>
                <td>{user.firstName + ' ' + user.lastName}</td>                
                <td>{user.email}</td>
                <td className="text-right">
                    <div className="btn-group">
                        <button type="button" className="btn btn-success">Edit</button>
                        <button type="button" className="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span className="sr-only">Toggle Dropdown</span>
                        </button>
                        <div className="dropdown-menu">
                            <span className="dropdown-item text-danger" onClick={() => {this.deleteUser(user.id)}}>Delete user</span>
                        </div>
                    </div>
                </td>
            </tr>
        )
    }

    render() {
        return (
            <div className="col-md-9 py-4 border-md-left">
                <h2 className="font-weight-bold mb-4">Reports</h2>
                <table className="table w-100">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Reports
