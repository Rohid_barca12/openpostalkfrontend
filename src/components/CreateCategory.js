//import react
import React, {Component} from 'react'
import { withRouter, Redirect } from "react-router-dom"

// import axios
import axios from 'axios';

class CreateCategory extends Component {

    constructor(props) {
        super()
        this.state = {
            name: null,
            description: null,
            subtalk: null,
            subtalkList: null,
            redirect: false,
            redirectId: null
        }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleCategoryCreate = this.handleCategoryCreate.bind(this)
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name
        let errors = this.state.errors

        this.setState({
            errors, [name]: value
        })
    }

    async handleCategoryCreate(event) {
        event.preventDefault()

        await axios.post('http://localhost:3000/category', {
                name: this.state.name,
                description: this.state.description
            },
            {
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token'),
                    'Content-Type': 'application/json'
                }
            })
        .then(response => {
            this.setState({
                redirect: true,
                redirectId: response.data.id
            })
        })
        .catch(error => {
            this.setState({
                error: error.response.data
            })

            if (this.state.error.statusCode !== 400) {
                console.log(this.state.error)
            }
        })
    }

    render() {

        if (this.state.redirect) {
            return <Redirect to={`/subtalk/${this.state.redirectId}`} />
        }

        return (
            <div className="col-md-9 border-md-left py-4">
                <h2 className="font-weight-bold mb-4">Create your own community!</h2>
                <form onSubmit={this.handleCategoryCreate}>
                    <div className="form-group">
                        <label className="m-0" htmlFor="name">Title</label>
                        <input className="form-control" type="text" id="name" name="name" placeholder="The name of your subtalk" onChange={this.handleInputChange} />
                    </div>
                    <div className="form-group">
                        <label className="m-0" htmlFor="description">Description</label>
                        <textarea className="form-control" id="description" name="description" placeholder="Give people a description of what they can expect to find on your subtalk" onChange={this.handleInputChange}></textarea>
                    </div>
                    <div className="form-group text-right">
                        <button className="btn btn-success btn-fill" type="submit">Create subtalk</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default withRouter(CreateCategory)
