// import react
import React, {Component} from 'react'
import { Link } from "react-router-dom"

// import axios
import axios from 'axios'

// import components
import PopularCommunitiesItem from './PopularCommunitiesItem'

// import data
import popularCommunitiesData from '../data/popularCommunitiesData'

class PopularCommunities extends Component {

    constructor() {
        super()

        this.state = {
            categories: []
        }

        this.getPopularCommunities = this.getPopularCommunities.bind(this)
    }

    async componentDidMount() {
        await axios.get(`http://localhost:3000/category`)
            .then(response => {
                this.setState({
                    categories: response.data
                })
                console.log(response.data)
            })
            .catch(error => {
                console.log(error)
            })
    }

    getPopularCommunities() {
        return this.state.categories.map((category, i) =>
            i <= 4 && <PopularCommunitiesItem key={category.id} category={category} />
        )
    }

    render() {
        return (
            <div className="sidebar bg-white my-3">
                <div className="title px-3 py-3">
                    <b className="text">Popular communities</b>
                </div>
                <div className="sidebar-body">
                    {this.getPopularCommunities()}
                </div>
                <div className="viewAll p-3">
                    <Link className="btn btn-primary btn-fill py-1 w-100" to="/communities">VIEW ALL</Link>
                </div>
            </div>
        )
    }
}

export default PopularCommunities
