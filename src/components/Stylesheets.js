//import react
import React, {Component} from 'react'

function Stylesheets(props) {
    if (this.props.style == 'dark') {
        require('../assets/dark.css').default
    } else {
        require('../assets/style.css').default
    }
}

export default Stylesheets
