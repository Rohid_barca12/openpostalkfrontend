import React, { Component } from "react";
// import dark from '../assets/dark.css';

class UserSettings extends Component {

    render() {
        return (
            <div className="col-md-9 py-4 border-md-left">
                <h2 className="font-weight-bold mb-4">Account settings</h2>

                <div className="d-flex align-items-center">
                    <label className="switch mr-3 mb-0">Dark theme</label>
                    <input type="checkbox" onClick={this.props.toggleStyle} checked={this.props.darkBool} />
                </div>
            </div>
        )
    }
}

export default UserSettings