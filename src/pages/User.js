//import react
import React, {Component} from 'react'
import {
    Link,
    Redirect
} from "react-router-dom"

// import components
import UserGeneral from '../components/UserGeneral'
import UserSettings from '../components/UserSettings'
import UserPasswordChange from '../components/UserPasswordChange'

class User extends Component {

    constructor() {
        super()

        this.toggleStyle = this.toggleStyle.bind(this)
    }

    toggleStyle() {
        this.props.handleStyleChange()
    }

    render() {

        const { handle, userId } = this.props.match.params

        if (localStorage.getItem('token') === null || localStorage.getItem('token') === '') {
            return (
                <Redirect to="/forbidden" />
            )
        } else {
            return (
                <main className="container my-5">
                    <div className="col-12 card">
                        <div className="userInfo row">
                            <div className="generalUser col-md-3 px-0">
                                <ul className="list-unstyled sideNav m-0 p-0">
                                    <Link to="/user/general">
                                        <li className={handle === 'general' ? 'active px-4 py-3' : 'px-4 py-3'}>General</li>
                                    </Link>
                                    <Link to="/user/settings">
                                        <li className={handle === 'settings' ? 'active px-4 py-3' : 'px-4 py-3'}>Settings</li>
                                    </Link>
                                    <Link to="/user/password">
                                        <li className={handle === 'password' ? 'active border-bottom-0 px-4 py-3' : 'border-bottom-0 px-4 py-3'}>Change password</li>
                                    </Link>
                                </ul>
                            </div>
                            {handle === 'general' && <UserGeneral userId={userId} />}
                            {handle === 'settings' && <UserSettings toggleStyle={this.toggleStyle} userId={userId} darkBool={this.props.darkBool} />}
                            {handle === 'password' && <UserPasswordChange userId={userId} />}
                        </div>
                    </div>
                </main>
            )
        }
    }
}

export default User
