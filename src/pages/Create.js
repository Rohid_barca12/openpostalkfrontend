// import react
import React from 'react'
import {
    Link,
    useParams,
    Redirect
} from "react-router-dom"

// import components
import CreatePost from '../components/CreatePost'
import CreateCategory from '../components/CreateCategory'

function Create() {

    const { handle } = useParams()

    if (localStorage.getItem('token') === null || localStorage.getItem('token') === '') {
        return (
            <Redirect to="/forbidden" />
        )
    } else {
        return (
            <main className="container my-5">
                <div className="col-12 card">
                    <div className="row">
                        <div className="col-md-3 px-0">
                            <ul className="list-unstyled sideNav m-0 p-0">
                                <Link to="/create/post">
                                    <li className={handle === 'post' ? 'active px-4 py-3' : 'px-4 py-3'}>Create post</li>
                                </Link>
                                <Link to="/create/subtalk">
                                    <li className={handle === 'subtalk' ? 'active px-4 py-3' : 'px-4 py-3'}>Create subtalk</li>
                                </Link>
                            </ul>
                        </div>
                        {handle === 'post' && <CreatePost />}
                        {handle === 'subtalk' && <CreateCategory />}
                    </div>
                </div>
            </main>
        )
    }
}

export default Create
