// import react
import React, {Component} from 'react'

class Forbidden extends Component {
    render() {
        return (
            <div className="text-center pt-5">
                <div className="mt-5">
                    <h1 className="font-weight-bold">Whoops, looks like you got no permission to be here.</h1>
                    <h2 className="h3 mt-4">
                        Please turn around. You've seen nothing.
                    </h2>
                </div>
            </div>
        )
    }
}

export default Forbidden
