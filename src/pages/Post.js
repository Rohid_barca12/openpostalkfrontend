// import react
import React, { Component } from 'react'
import {
    Link
} from "react-router-dom"

// import axios
import axios from 'axios'

class Post extends Component {

    constructor() {
        super()

        this.state = {
            post: '',
            user: '',
            category: '',
            comments: [],
            commentContent: '',
            commentUser: '',
            updated: false,
            commentBlock: false
        }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleCommentCreate = this.handleCommentCreate.bind(this)
        this.getComments = this.getComments.bind(this)
        this.updateComments = this.updateComments.bind(this)
    }

    async componentDidMount() {
        const { id } = this.props.match.params

        await axios.get(`http://localhost:3000/post/${id}?entities=1`, {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem('token'),
                "Content-Type": "application/json"
            }})
            .then(response => {
                this.setState({
                    post: response.data,
                    user: response.data.user,
                    category: response.data.category,
                    comments: response.data.comments
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    async updateComments() {
        const { id } = this.props.match.params

        await axios.get(`http://localhost:3000/post/${id}?entities=1`, {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem('token'),
                "Content-Type": "application/json"
            }})
            .then(response => {
                this.setState({
                    comments: response.data.comments
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name
        let errors = this.state.errors

        this.setState({
            errors, [name]: value
        })
    }

    async handleCommentCreate(event) {
        event.preventDefault()
        
        await axios.post('http://localhost:3000/comment', {
                content: this.state.commentContent,
                postId: this.state.post.id
            },
            {
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token'),
                    'Content-Type': 'application/json'
                }
            })
        .then(response => {
            this.updateComments()
        })
        .catch(error => {
            this.setState({
                error: error.response.data,
                commentContent: ''
            })

            if (this.state.error.statusCode !== 400) {
                console.log(this.state.error)
            }
        })
    }

    getPostedTime(createdAt) {
        const postedTime = new Date(createdAt).getTime()
        const now = new Date().getTime()
        
        const ms = now - postedTime

        // Calculating Seconds 
        var sec = (ms / 1000).toFixed(0)
  
        // Calculating Minutes 
        var min = (ms / (1000 * 60)).toFixed(0)

        // Calculating hours  
        var hrs = (ms / (1000 * 60 * 60)).toFixed(0)

        // Calculating days 
        var days = (ms / (1000 * 60 * 60 * 24)).toFixed(0)
        if (sec < 60) { 
            return sec + " seconds ago"
        } else if (min < 60) { 
            return min + " minutes ago"
        } else if (hrs < 24) { 
            return hrs + " hours ago"
        } else { 
            return days + " days ago"
        } 
    }

    getComments() {
        if (this.state.comments.length > 0) {
            return this.state.comments.map(comment =>
                <div className="content border-bottom w-100 p-3" key={comment.id}>
                    <div className="d-flex justify-content-start align-items-center">
                        <div className="ml-2">
                            <div className="content-info">
                                <span className="small">
                                    Posted {this.getPostedTime(comment.createdAt)}
                                </span>
                            </div>
                            <div className="content-body">
                                {comment.content}
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return <div className="h4 text-center p-4">There are no comments yet. Be the first to speak your mind!</div>
        }
    }

    render() {
        return (
            <main className="container my-5">
                <div className="content-container no-hover d-flex my-3">
                    <div className="post-votes d-flex flex-column align-items-center text-center py-2"></div>
                    <div className="content p-2">
                        <div className="content-info d-flex justify-content-between align-items-center mb-2">
                            <div>
                                <b><Link to={`/subtalk/${this.state.category.id}`} >{this.state.category.name}</Link> </b>
                                <span className="small">
                                    &bull; Posted by {this.state.user.username}
                                </span>
                            </div>
                        </div>
                        <div className="content-body py-2">
                            <h3 className="h5 font-weight-bold">{this.state.post.title}</h3>
                            <p>
                                {this.state.post.content}
                            </p>
                        </div>
                    </div>
                </div>

                <div className="content-container no-hover rounded px-4 py-3 mt-5">
                    <form onSubmit={this.handleCommentCreate}>
                        <div className="form-group">
                            <label className="m-0" htmlFor="commentContent">Comment</label>
                            <textarea className="form-control" id="commentContent" name="commentContent" placeholder="Let us know what you think about this post" onChange={this.handleInputChange} value={this.state.commentContent}></textarea>
                        </div>
                        <div className="form-group text-right">
                            <button className="btn btn-success btn-fill" type="submit">Post comment</button>
                        </div>
                    </form>
                    {this.state.commentBlock && <span className="alert alert-danger" role="alert">You have already commented in this session.</span>}
                </div>

                <div className="content-container no-hover rounded my-3">
                    {this.getComments()}
                </div>
            </main>
        )
    }
}

export default Post
