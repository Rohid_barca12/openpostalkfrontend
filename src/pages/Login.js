//import react
import React, {Component} from 'react'
import {
    Link,
    Redirect
} from "react-router-dom"

// import axios
import axios from 'axios';

// import assets
import logo from '../img/logo.png'

class Login extends Component {
    
    constructor()
    {
        super()
        this.state = {
            loading: false,
            redirect: false,
            loginFailed: false,
            username: '',
            password: ''
        }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleLogin = this.handleLogin.bind(this)
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value
        })
    }

    async handleLogin(event) {
        event.preventDefault()
        this.setState({
            loading: true,
        })
        await axios.post('http://localhost:3000/auth/login', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                this.setState({
                    redirect: true,
                    loginFailed: false
                })
                localStorage.setItem('token', response.data.access_token)
                this.props.handleIsLoggedIn()
            })
            .catch(error => {
                this.setState({
                    loading: false,
                    loginFailed: true
                })
            })
    }

    render() {
        
        if (this.state.redirect) {
            return <Redirect to="/" />
        }

        return (
            <main className="container my-5">
                <div className="card p-5">
                    <div className="row d-flex">
                        <div className="d-none d-lg-block col-lg-6 position-relative">
                            <div className="text-center logo-center">
                                <img className="w-50" src={logo} alt="Logo Postalk" />
                            </div>
                        </div>
                        <div className="col-lg-6">
                            {
                                this.state.loginFailed &&
                                    <div className="alert alert-danger" role="alert">
                                        Incorrect username or password. Please try again.
                                    </div>
                            }
                            <form onSubmit={this.handleLogin}>
                                <div className="form-group">
                                    <label className="m-0" htmlFor="username">Username</label>
                                    <input className="form-control" type="text" id="username" name="username" onChange={this.handleInputChange} />
                                </div>
                                <div className="form-group">
                                    <label className="m-0" htmlFor="password">Password</label>
                                    <input className="form-control" type="password" id="password" name="password" onChange={this.handleInputChange} />
                                </div>
                                <div className="form-group text-right">
                                    {
                                        this.state.loading ?
                                            <button className="btn btn-success btn-fill" type="submit" disabled><i className="fas fa-spinner fa-spin mr-1"></i> Log in</button>
                                        :
                                            <button className="btn btn-success btn-fill" type="submit">Log in</button>
                                    }
                                </div>
                            </form>
                            <hr />
                            <div className="text-center">
                                <p>Don't have an account yet? <Link to="/register">Create one!</Link></p>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}

export default Login
