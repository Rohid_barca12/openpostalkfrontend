//import react
import React, {Component} from 'react'

class NotFound extends Component {

    render() {
        return (
            <div className="text-center mt-5">
                <h1 className="display-1 font-weight-bold">404</h1>
                <p className="h3">
                    The page you're looking for doesn't seem to exist in our records.
                </p>
            </div>
        )
    }
}

export default NotFound
