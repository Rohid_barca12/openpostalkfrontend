//import react
import React from 'react'
import { useParams } from "react-router-dom"

function Error() {

    const { handle } = useParams()

    return (
        <div>
            {
                handle === 401 &&
                <div>
                    <h1>{handle}</h1>
                    <p>
                        Sorry, we did not receive the needed authentication for you to visit this page.
                    </p>
                </div>
            }
        </div>
    )
}

export default Error
