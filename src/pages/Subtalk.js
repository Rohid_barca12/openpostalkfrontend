//import react
import React, {Component} from 'react'

// import axios
import axios from 'axios';

// import components
import Post from '../components/Post'
import PostSorter from '../components/PostSorter'
import SubtalkDescription from '../components/SubtalkDescription'

import userData from '../data/userData'

class Subtalk extends Component {

    constructor() {
        super()
        this.state = {
            name: null,
            description: null,
            posts: []
        }

        this.handleSortBy = this.handleSortBy.bind(this)
        this.getPosts = this.getPosts.bind(this)
    }

    async componentDidMount() {
        const { id } = this.props.match.params

        await axios.get(`http://localhost:3000/post/all/${id}`, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/x-www-form-urlencoded'
            }})
            .then(response => {
                this.setState({
                    posts: response.data
                })
            })
            .catch(error => {
                console.log(error)
            })

        await axios.get(`http://localhost:3000/category/${id}`, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/x-www-form-urlencoded'
            }})
            .then(response => {
                this.setState({
                    name: response.data.name,
                    description: response.data.description
                })
                console.log(this.state.name)
            })
            .catch(error => {
                console.log(error)
            })
    }

    handleSortBy(sort) {
        this.setState({
            sortBy: sort
        })
    }

    getPosts() {
        if (this.state.posts.length > 0) {
            return this.state.posts.map(post =>
                <Post key={post.id} post={post} user={userData} pointsChange={this.pointsChange} followSubtalk={this.followSubtalk} />
            )
        } else {
            return <div className="h4 text-center mt-5">There hasn't been posted anything to this subtalk yet.</div>
        }
    }

    render() {
        if (this.state.name !== undefined) {
            return (
                <main className="container my-5">
                    <h1 className="text-center font-weight-bold mb-5">{this.state.name}</h1>
                    <div className="row">
                        <div className="col-md-8 col-lg-9">
                            {this.getPosts()}
                        </div>
                        <div className="d-none d-md-block col-4 col-lg-3">
                            <SubtalkDescription subtalk={this.state} />
                        </div>
                    </div>
                </main>
            )
        } else {
            return (
                <main className="container my-5">
                    <h1 className="h2 text-center font-weight-bold mb-5">Sorry, but this subtalk doesn't seem to exist.</h1>
                </main>
            )
        }
    }
}

export default Subtalk
