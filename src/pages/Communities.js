// import react
import React, {Component} from 'react'
import { Link } from "react-router-dom"

// import axios
import axios from 'axios'

// import components
import PopularCommunitiesItem from '../components/PopularCommunitiesItem'

class Communities extends Component {

    constructor() {
        super()
        this.state = {
            categories: []
        }
    }

    async componentDidMount() {
        await axios.get(`http://localhost:3000/category`)
            .then(response => {
                this.setState({
                    categories: response.data
                })
                console.log(response.data)
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        return (
            <main className="container my-5">
                <h1 className="font-weight-bold text-center mb-4">All our communities</h1>
                <div className="content-container no-hover">
                    <div className="content w-100 p-0">
                        {
                            this.state.categories.map(category =>
                                <Link to={`/subtalk/${category.id}`} key={category.id}>
                                    <div className="list-item p-3">
                                        {category.name}
                                    </div>
                                </Link>
                            )
                        }
                    </div>
                </div>
            </main>
        )
    }
}

export default Communities
