// import react
import React, {Component} from 'react'
import {
    Link,
    Redirect
} from "react-router-dom"

// import axios
import axios from 'axios'

// import assets
import logo from '../img/logo.png'


const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)

class Register extends Component {

    constructor() {
        super()
        this.state = {
            firstName: '',
            lastName: '',
            username: '',
            email: '',
            password: '',
            confirmPassword: '',
            loading: false,
            registerSuccess: false,
            errors: {
                firstNameLength: false,
                lastNameLength: false,
                usernameLength: false,
                emailValid: false,
                passwordLength: false,
                confirmPasswordValid: false
            },
            errorMessage: null,
            statusCode: null,
            redirect: false
        }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.validate = this.validate.bind(this)
        this.handleRegister = this.handleRegister.bind(this)
        this.handleLogin = this.handleLogin.bind(this)
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value
        })
    }

    validate(firstName, lastName, username, email, password, confirmPassword) {
        return {
            firstName: firstName.length >= 2
                ? false
                : true,
            lastName: lastName.length >= 2
                ? false
                : true,
            username: username.length >= 6
                ? false
                : true,
            email: validEmailRegex.test(email)
                ? false
                : true,
            password: password.length >= 6
                ? false
                : true,
            confirmPassword: password === confirmPassword
                ? false
                : true,
        }
    }

    async handleRegister(event) {
        event.preventDefault()

        const errors = this.validate(this.state.firstName, this.state.lastName, this.state.username, this.state.email, this.state.password, this.state.confirmPassword)

        if (!errors.firstName && !errors.lastName && !errors.username && !errors.email && !errors.password && !errors.confirmPassword) {
            this.setState({
                loading: true,
            })

            await axios.post('http://localhost:3000/auth/register', {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    username: this.state.username,
                    email: this.state.email,
                    password: this.state.password,
                    confirmPassword: this.state.confirmPassword
                })
                .then(response => {
                    console.log(response)
                    this.handleLogin()
                })
                .catch(error => {
                    this.setState({
                        loading: false,
                        errorMessage: error.response.data.message,
                        statusCode: error.response.data.statusCode
                    })

                    const statusCode = this.state.statusCode
        
                    if (statusCode !== 400) {
                        this.props.history.push(`/error/${statusCode}`)
                    }
                })
        } else {
            this.setState({
                errors: {
                    firstNameLength: errors.firstName,
                    lastNameLength: errors.lastName,
                    usernameLength: errors.username,
                    emailValid: errors.email,
                    passwordLength: errors.password,
                    confirmPasswordValid: errors.confirmPassword
                }
            })
        }
    }

    async handleLogin() {
        this.setState({
            loading: true,
        })
        await axios.post('http://localhost:3000/auth/login', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                this.setState({
                    redirect: true,
                    loginFailed: false
                })
                localStorage.setItem('token', response.data.access_token)
                this.props.handleIsLoggedIn()
            })
            .catch(error => {
                this.setState({
                    loading: false,
                    loginFailed: true
                })
            })
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to="/" />
        }

        return (
            <main className="container my-5">
                <div className="card p-5">
                    <div className="row d-flex">
                        <div className="d-none d-lg-block col-lg-6 position-relative">
                            <div className="text-center logo-center">
                                <img className="w-50" src={logo} alt="Logo Postalk" />
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <form onSubmit={this.handleRegister}>
                                <div className="form-group">
                                    <label className="m-0" htmlFor="firstName">First name</label>
                                    <input className={this.state.errors.nameLength ? "form-control error" : "form-control"} type="text" id="firstName" name="firstName" onChange={this.handleInputChange} onBlur={this.handleInputOnBlur} />
                                    <span className="errorMsg">{this.state.errors.firstNameLength && 'First name is required.'}</span>
                                </div>
                                <div className="form-group">
                                    <label className="m-0" htmlFor="lastName">Last name</label>
                                    <input className={this.state.errors.nameLength ? "form-control error" : "form-control"} type="text" id="lastName" name="lastName" onChange={this.handleInputChange} onBlur={this.handleInputOnBlur} />
                                    <span className="errorMsg">{this.state.errors.lastNameLength && 'Last name is required.'}</span>
                                </div>
                                <div className="form-group">
                                    <label className="m-0" htmlFor="username">Username</label>
                                    <input className={this.state.errors.usernameLength ? "form-control error" : "form-control"} type="text" id="username" name="username" onChange={this.handleInputChange} onBlur={this.handleInputOnBlur} />
                                    <span className="errorMsg">{this.state.errors.usernameLength && 'Username is required and should be at least 6 characters.'}</span>
                                </div>
                                <div className="form-group">
                                    <label className="m-0" htmlFor="email">E-mail</label>
                                    <input className={this.state.errors.emailValid ? "form-control error" : "form-control"} type="test" id="email" name="email" onChange={this.handleInputChange} onBlur={this.handleInputOnBlur} />
                                    <span className="errorMsg">{this.state.errors.emailValid && 'Please use a valid e-mailaddress.'}</span>
                                </div>
                                <div className="form-group">
                                    <label className="m-0" htmlFor="password">Password</label>
                                    <input className={this.state.errors.passwordLength ? "form-control error" : "form-control"} type="password" id="password" name="password" onChange={this.handleInputChange} onBlur={this.handleInputOnBlur} />
                                    <span className="errorMsg">{this.state.errors.passwordLength && 'Password is required and should be at least 6 characters.'}</span>
                                </div>
                                <div className="form-group">
                                    <label className="m-0" htmlFor="confirmPassword">Confirm password</label>
                                    <input className={this.state.errors.confirmPasswordValid ? "form-control error" : "form-control"} type="password" id="confirmPassword" name="confirmPassword" onChange={this.handleInputChange} onBlur={this.handleInputOnBlur} />
                                    <span className="errorMsg">{this.state.errors.confirmPasswordValid && 'Passwords don\'t match.'}</span>
                                </div>
                                <div className="form-group text-right">
                                    {
                                        this.state.loading ?
                                            <button className="btn btn-success btn-fill" type="submit" disabled><i className="fas fa-spinner fa-spin mr-1"></i> Register</button>
                                        :
                                            <button className="btn btn-success btn-fill" type="submit">Register</button>
                                    }
                                </div>
                            </form>
                            <hr />
                            <div className="text-center">
                                <p>Already have an account? <Link to="/login">Log in</Link></p>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}

export default Register
