//import react
import React from 'react'
import {
    Link,
    useParams
} from "react-router-dom"

// import components
import Users from '../components/Users'
import Reports from '../components/Reports'
import EmailTemplate from '../components/EmailTemplate'

function Admin () {

    const { handle } = useParams()

    return (
        <main className="container my-5">
            <div className="col-12 card">
                <div className="row">
                    <div className="col-md-3 px-0">
                        <ul className="list-unstyled sideNav m-0 p-0">
                            <Link to="/admin/users">
                                <li className={handle === 'users' ? 'active px-4 py-3' : 'px-4 py-3'}>Users</li>
                            </Link>
                            <Link to="/admin/reports">
                                <li className={handle === 'reports' ? 'active px-4 py-3' : 'px-4 py-3'}>Reports</li>
                            </Link>
                            <Link to="/admin/emailtemplate">
                                <li className={handle === 'emailtemplate' ? 'active px-4 py-3' : 'px-4 py-3'}>Email Template</li>
                            </Link>
                        </ul>
                    </div>
                    {handle === 'users' && <Users />}
                    {handle === 'reports' && <Reports />}
                    {handle === 'emailtemplate' && <EmailTemplate />}
                </div>
            </div>
        </main>
    )
}

export default Admin
