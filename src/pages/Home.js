// import react
import React, {Component} from 'react'

// import axios
import axios from 'axios'

// import components
import Post from '../components/Post'
import PopularCommunities from '../components/PopularCommunities'

// import data
import postData from '../data/postData'
import userData from '../data/userData'

class Home extends Component {

    constructor() {
        super()
        this.state = {
            posts: [],
            user: userData,
            sortBy: 'Top'
        }

        this.pointsChange = this.pointsChange.bind(this)
        this.followSubtalk = this.followSubtalk.bind(this)
        this.handleSortBy = this.handleSortBy.bind(this)
        this.getPosts = this.getPosts.bind(this)
    }

    async componentDidMount() {
        await axios.get('http://localhost:3000/post', {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem('token'),
                "Content-Type": "application/json"
            }})
            .then(response => {
                this.setState({
                    posts: response.data
                })
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
    }

    pointsChange(id, action) {
        this.setState(prevState => {
            const updatedPosts = prevState.posts.map(post => {
                if (post.id === id) {
                    action === 'add' ? post.points++ : post.points--
                }
                return post
            })
            return {
                posts: updatedPosts
            }
        })
    }

    followSubtalk(id) {
        if (localStorage.getItem('token') !== null && localStorage.getItem('token') !== '') {
            this.setState(prevState => {
                prevState.user[0].subTalks.push(id)
                return {
                    user: prevState.user
                }
            })
        }
    }

    handleSortBy(sort) {
        this.setState({
            sortBy: sort
        })
    }

    getPosts() {
        return this.state.posts.map(post =>
            <Post key={post.id} post={post} user={userData} pointsChange={this.pointsChange} followSubtalk={this.followSubtalk} />
        )
    }

    render() {
        return (
            <main className="container my-5">
                <div className="row">
                    <div className="col-md-8 col-lg-9">
                        {this.getPosts()}
                    </div>
                    <div className="d-none d-md-block col-4 col-lg-3">
                        <PopularCommunities />
                    </div>
                </div>
            </main>
        )
    }
}

export default Home
